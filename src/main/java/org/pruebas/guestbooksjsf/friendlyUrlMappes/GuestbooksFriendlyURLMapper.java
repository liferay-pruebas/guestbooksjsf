package org.pruebas.guestbooksjsf.friendlyUrlMappes;

import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;


public class GuestbooksFriendlyURLMapper extends DefaultFriendlyURLMapper {

    @Override
    public String getMapping() {
        return _MAPPING;
    }

    private static final String _MAPPING = "jsfguestbooks";

}
