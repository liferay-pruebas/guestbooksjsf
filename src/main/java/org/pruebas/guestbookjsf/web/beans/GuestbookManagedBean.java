package org.pruebas.guestbookjsf.web.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.pruebas.guestbookjsf.serviceTracker.EntryLocalServiceTracker;
import org.pruebas.guestbookjsf.serviceTracker.GuestbookLocalServiceTracker;

import com.liferay.docs.guestbook.model.Entry;
import com.liferay.docs.guestbook.model.Guestbook;
import com.liferay.docs.guestbook.service.EntryLocalService;
import com.liferay.docs.guestbook.service.GuestbookLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;

@ManagedBean(name = "guestbookMB")
@ViewScoped
public class GuestbookManagedBean implements Serializable {

	private List<Guestbook> listGuestbooks;
	private long selectedGuestbookId = 0;

	// Servicios
	private GuestbookLocalServiceTracker gestbookServiceTracker;
	private EntryLocalServiceTracker entryServiceTracker;
	
	
	//Prueba ajax
	private String input;
	private String output;

	@ManagedProperty(value = "#{sesionMB}")
	private SesionManagedBean sesionMB;

	public void setSesionMB(SesionManagedBean sesionMB) {
		this.sesionMB = sesionMB;
	}

	@PostConstruct
	public void inicialiceServiceTracker() {
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		BundleContext bundleContext = bundle.getBundleContext();
		this.gestbookServiceTracker = new GuestbookLocalServiceTracker(bundleContext);
		this.gestbookServiceTracker.open();
		this.entryServiceTracker = new EntryLocalServiceTracker(bundleContext);
		this.entryServiceTracker.open();
	}

	@PreDestroy
	public void closeServiceTracker() {
		this.gestbookServiceTracker.close();
		this.entryServiceTracker.close();
		System.out.println("Servicios cerrados en GuestbookMB");
	}

	public long getSelectedGuestbookId() {
		return selectedGuestbookId;
	}
	
	public String getInput() {
		return input;
	}
	
	public String getOutput() {
		return output;
	}
	
	public void setInput(String input) {
		this.input = input;
	}
	
	public void setOutput(String output) {
		this.output = output;
	}

	public List<Guestbook> getListGuestbooks() {
		GuestbookLocalService gbService = this.gestbookServiceTracker.getService();
		ServiceContext sctx;

		if (gbService == null) {
			System.out.println("El servicio guestbook  es nulo");
			return null;
		}

		try {
			sctx = ServiceContextFactory
					.getInstance((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
			this.listGuestbooks = gbService.getGuestbooks(sctx.getScopeGroupId());
			
			if (this.selectedGuestbookId == 0) {
				if (sesionMB.getGuestbookId() == 0)
					this.selectedGuestbookId = this.listGuestbooks.get(0).getGuestbookId();
				else
					this.selectedGuestbookId = sesionMB.getGuestbookId();
			}
			
			return this.listGuestbooks;
		} catch (PortalException e) {
			System.out.println("Excepcion de getListGuestbooks");
			e.printStackTrace();
		}
		return null;
	}

	public List<Entry> getListEntryFromGuestbook() {
		EntryLocalService enService = this.entryServiceTracker.getService();
		ServiceContext sctx;

		if (listGuestbooks != null || listGuestbooks.isEmpty() && this.selectedGuestbookId != 0) {

			try {
				sctx = ServiceContextFactory.getInstance(
						(PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
				List<Entry> l = enService.getEntries(sctx.getScopeGroupId(), this.selectedGuestbookId);
				// System.out.println("lista de entradas del guestbook: " + l);
				return l;
			} catch (PortalException e) {
				System.out.println("Excepcion de getListEntryFromGuestbook");
				e.printStackTrace();
			}
		} else {
			// TODO Error
		}

		return null;
	}

	public String editEntry(Entry e) {
		this.sesionMB.setModifyEntry(true);
		this.sesionMB.setEntry(e);

		return "editar";
	}

	public String addNewEntry() {
		this.sesionMB.setModifyEntry(false);
		this.sesionMB.setEntry(null);
		this.sesionMB.setGuestbookId(this.selectedGuestbookId);

		return "nuevo";
	}

	public String changeGuestbookId(long id) {
		this.selectedGuestbookId = id;
		return "";
	}
	
	
	public void deleteEntry(long entryId){
		FacesContext fc = FacesContext.getCurrentInstance();
		EntryLocalService enService = this.entryServiceTracker.getService();
		ServiceContext sctx;

		try {
			sctx = ServiceContextFactory.getInstance((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
			enService.deleteEntry(entryId, sctx);
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Entry deleted", "The entry has been deleted from DB"));
			//System.out.println(LanguageUtil.get(FacesContext.getCurrentInstance().getExternalContext().getRequestLocale(), "GuestbookJSF-hello-world"));
		} catch (PortalException e) {
			System.out.println("Excepcion de getListEntryFromGuestbook");
			e.printStackTrace();
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error deleting", "The entry could not be deleted"));
		}

	}
	
	public void ajaxListener(AjaxBehaviorEvent e){
		System.out.println("Listener ajax");
		System.out.println(input);
	}

}
