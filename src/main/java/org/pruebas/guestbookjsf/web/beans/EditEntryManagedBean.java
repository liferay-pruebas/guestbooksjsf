package org.pruebas.guestbookjsf.web.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.portlet.PortletRequest;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.pruebas.guestbookjsf.serviceTracker.EntryLocalServiceTracker;
import org.pruebas.guestbookjsf.serviceTracker.GuestbookLocalServiceTracker;

import com.liferay.docs.guestbook.model.Entry;
import com.liferay.docs.guestbook.service.EntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;

@ManagedBean(name = "editEntryMB")
@RequestScoped
public class EditEntryManagedBean {

	private String name;
	private String email;
	private String message;
	private long entryId;
	private long guestbookId;

	// Service
	private EntryLocalServiceTracker entryServiceTracker;

	@ManagedProperty(value = "#{sesionMB}")
	private SesionManagedBean sesionMB;

	@PostConstruct
	public void inicializarEntrada() {
		if (sesionMB.isModifyEntry() && sesionMB.getEntry() != null) {
			this.name = sesionMB.getEntry().getName();
			this.email = sesionMB.getEntry().getEmail();
			this.message = sesionMB.getEntry().getMessage();
			this.entryId = sesionMB.getEntry().getEntryId();
			this.guestbookId = sesionMB.getEntry().getGuestbookId();
			sesionMB.setEntry(null);
		}else{
			this.guestbookId = sesionMB.getGuestbookId();
		}
		
		inicialiceServiceTracker();
	}
	
	public void inicialiceServiceTracker(){
		System.out.println("Se ejecuta initservtrack");
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
        BundleContext bundleContext = bundle.getBundleContext();
        this.entryServiceTracker = new EntryLocalServiceTracker(bundleContext);
        this.entryServiceTracker.open();
	}
	
	@PreDestroy
	public void closeServiceTracker(){
		this.entryServiceTracker.close();
		System.out.println("Servicios cerrados en EditEntryMB");
	}

	public void setSesionMB(SesionManagedBean sesionMB) {
		this.sesionMB = sesionMB;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getMessage() {
		return message;
	}
	
	public long getEntryId() {
		return entryId;
	}
	
	public long getGuestbookId() {
		return guestbookId;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}

	public void setGuestbookId(long guestbookId) {
		this.guestbookId = guestbookId;
	}

	public String saveEntry() {
		FacesContext fc = FacesContext.getCurrentInstance();
		EntryLocalService enService = this.entryServiceTracker.getService();
		ServiceContext sctx;

		try {
			sctx = ServiceContextFactory.getInstance((PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());

			if (sesionMB.isModifyEntry()) {
				// Modify existing entry
				enService.updateEntry(sctx.getGuestOrUserId(), this.guestbookId, this.entryId, this.name, this.email, this.message, sctx);
				fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Entry modified", "The entry has been modified in DB"));
			} else {
				// Create new Entry
				enService.addEntry(sctx.getGuestOrUserId(), this.guestbookId, this.name, this.email, this.message, sctx);
				fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Entry added", "The entry has been saved in DB"));
			}
		} catch (SystemException | PortalException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error saving the entry", "The entry could not be saved"));
			return "";
		}

		return "home";
	}

	public String cancel() {
		sesionMB.setModifyEntry(false);
		return "home";//?faces-redirect=true";
	}
}
