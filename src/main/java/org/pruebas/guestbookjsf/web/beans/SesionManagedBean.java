package org.pruebas.guestbookjsf.web.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.liferay.docs.guestbook.model.Entry;

@ManagedBean(name = "sesionMB")
@SessionScoped
public class SesionManagedBean implements Serializable{

	private boolean modifyEntry;
	private Entry entry;
	private long guestbookId;
	
	public SesionManagedBean() {
		System.out.println("+++++SessionManagedBean Constructor");
	}
	
	public void setModifyEntry(boolean modifyEntry) {
		this.modifyEntry = modifyEntry;
	}
	
	public boolean isModifyEntry() {
		return modifyEntry;
	}
	
	public void setEntry(Entry entry) {
		this.entry = entry;
	}
	
	public Entry getEntry() {
		return entry;
	}
	
	public void setGuestbookId(long guestbookId) {
		this.guestbookId = guestbookId;
	}
	
	public long getGuestbookId() {
		return guestbookId;
	}
}
