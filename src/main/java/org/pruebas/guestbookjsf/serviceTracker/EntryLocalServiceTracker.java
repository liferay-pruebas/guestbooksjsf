package org.pruebas.guestbookjsf.serviceTracker;

import java.io.Serializable;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.docs.guestbook.service.EntryLocalService;

public class EntryLocalServiceTracker extends ServiceTracker<EntryLocalService, EntryLocalService> implements Serializable{

    public EntryLocalServiceTracker(BundleContext bundleContext) {
        super(bundleContext, EntryLocalService.class, null);
    }
}