package org.pruebas.guestbookjsf.serviceTracker;

import java.io.Serializable;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.docs.guestbook.service.GuestbookLocalService;

public class GuestbookLocalServiceTracker extends ServiceTracker<GuestbookLocalService, GuestbookLocalService> implements Serializable{

    public GuestbookLocalServiceTracker(BundleContext bundleContext) {
        super(bundleContext, GuestbookLocalService.class, null);
    }
}